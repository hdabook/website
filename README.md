Website for *Humanities Data Analysis: Case Studies with Python*
======================================

This is the repository for the website accompanying the book *Humanities Data Analysis:
Case Studies with Python*, which is a practical guide to data-intensive humanities
research using the Python programming language. The book, written by Folgert Karsdorp,
Mike Kestemont and Allen Riddell, was originally published with Princeton University Press
in 2021 (for a printed version of the book, see the publisher’s website), and is now
available as an Open Access interactive Juptyer Book at
https://www.humanitiesdataanalysis.org. 

We invite readers of the book to report any bugs, errors, or other issues in this
repository. When doing so, please clearly describe the problem, the version of Python with
which the problem occurs, as well as the version numbers of the packages used. 
